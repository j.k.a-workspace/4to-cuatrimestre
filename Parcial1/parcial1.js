const readline = require('readline');
import('node-fetch').then((fetch) => {}).catch((error) => {
    console.error('Error al importar el módulo node-fetch:', error);
  });
  

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function mostrarMenu() {
  console.log('\n1. Todos los pendientes (solo IDs)');
  console.log('2. Todos los pendientes (IDs y Titles)');
  console.log('3. Pendientes sin resolver (ID y Title)');
  console.log('4. Pendientes resueltos (ID y Title)');
  console.log('5. Todos los pendientes (IDs y userID)');
  console.log('6. Pendientes resueltos (ID y userID)');
  console.log('7. Pendientes sin resolver (ID y userID)');
  console.log('8. Salir\n');
}

async function obtenerPendientes() {
  try {
    const response = await fetch('http://jsonplaceholder.typicode.com/todos');
    const data = await response.json();
    return data;
  } catch (error) {
    console.error('Fallo al obtener la lista de pendientes:', error);
    return [];
  }
}

function pendientesSinResolver(pendientes) {
  const noResueltos = pendientes.filter(p => !p.completed);
  console.log('Pendientes sin resolver (ID y Title):');
  noResueltos.forEach(p => console.log(`ID: ${p.id}, Title: ${p.title}`));
}

function mostrarPendientesResueltos(pendientes) {
  const resueltos = pendientes.filter(p => p.completed);
  console.log('Pendientes resueltos (ID y Title):');
  resueltos.forEach(p => console.log(`ID: ${p.id}, Title: ${p.title}`));
}

function mostrarIDsYUserIDPendientes(pendientes) {
  console.log('Todos los pendientes (IDs y userID):');
  pendientes.forEach(p => console.log(`ID: ${p.id}, UserID: ${p.userId}`));
}

async function menu() {
  mostrarMenu();

  rl.question('Seleccione una opción del menú (1-8): ', async (opcion) => {
    const pendientes = await obtenerPendientes();

    switch (opcion) {
      case '1':
        console.log('\nTodos los pendientes (solo IDs):');
        pendientes.forEach(p => console.log(`ID: ${p.id}`));
        break;
      case '2':
        console.log('\nTodos los pendientes (IDs y Titles):');
        pendientes.forEach(p => console.log(`ID: ${p.id}, Title: ${p.title}`));
        break;
      case '3':
        pendientesSinResolver(pendientes);
        break;
      case '4':
        mostrarPendientesResueltos(pendientes);
        break;
      case '5':
        mostrarIDsYUserIDPendientes(pendientes);
        break;
      case '6':
        mostrarPendientesResueltos(pendientes);
        break;
      case '7':
        pendientesSinResolver(pendientes);
        break;
      case '8':
        console.log('Terminando proceso...');
        rl.close();
        break;
      default:
        console.log('Opción inválida. Seleccione una opción del 1-8.');
        break;
    }

    if (opcion !== '8') {
      menu();
    }
  });
}

menu();

//pruebas
